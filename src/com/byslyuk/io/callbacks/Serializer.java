package com.byslyuk.io.callbacks;

import java.io.*;

public class Serializer {
    private  String filePath;
    public Serializer(String filePath) {
        this.filePath = filePath;
    }
    public  void serialise (String contentsFile){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(filePath))))  {
            oos.writeObject(contentsFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String deserialise() throws IOException, ClassNotFoundException {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(filePath)))) {
            String student = (String) ois.readObject();
            return student;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
