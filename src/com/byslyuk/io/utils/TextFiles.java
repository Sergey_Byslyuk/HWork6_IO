package com.byslyuk.io.utils;

/**
 * Created by user on 28.09.2017.
 */
public class TextFiles {
    private String nameFile;
    private String contentsFile;

    public TextFiles(String nameFile, String contentsFile) {
        this.nameFile = nameFile;
        this.contentsFile = contentsFile;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getContentsFile() {
        return contentsFile;
    }

    public void setContentsFile(String contentsFile) {
        this.contentsFile = contentsFile;
    }
}
