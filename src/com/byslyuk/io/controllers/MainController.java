package com.byslyuk.io.controllers;

import com.byslyuk.io.callbacks.Serializer;
import com.byslyuk.io.utils.TextFiles;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.09.2017.
 */
public class MainController implements Initializable {
    @FXML
    public TextField nameFile;
    @FXML
    public TextField contentFile;
    @FXML
    public TextArea commonText;

    public Serializer superSerializer;

    public String line = "";
    String common = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

         superSerializer = new Serializer("CommonFile.txt");

    }
    @FXML
    public void createFile(ActionEvent actionEvent) {
        TextFiles textFiles = new TextFiles(nameFile.getText(), contentFile.getText());
        Serializer serializer = new Serializer(textFiles.getNameFile()+".txt");
        serializer.serialise(textFiles.getContentsFile());
        line = textFiles.getContentsFile();
        addInCommonFile(serializer, line);
        nameFile.setText("");
        contentFile.setText("");

    }
    public void addInCommonFile(Serializer serializer, String line) {
            String d ="";
        try {
            d = serializer.deserialise();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        superSerializer.serialise(common + "\n" +d);
        common+=d;}
    @FXML

    public void showCommonFile(ActionEvent actionEvent) {
        String c ="";
        try {
            c= superSerializer.deserialise();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        commonText.setText(c);
    }
}
